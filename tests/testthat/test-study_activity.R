# WARNING - Generated by {fusen} from dev/flat_study_squirrels.Rmd: do not edit by hand

test_that("study_activity works", {

  data(data_act_squirrels)
  result <- study_activity(df_squirrels_act = data_act_squirrels, 
               col_primary_fur_color = "Gray")
  
  expect_true(inherits(result, "list"))
  
  expect_equal(
    names(result),
    c("table", "graph")
    )
  
  expect_error(
    study_activity(df_squirrels_act="Squirrels", col_primary_fur_color="Gray"),
    "df_squirrels_act is not a data frame")
  
})
